using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_GameManagement : MonoBehaviour
{
    public GameObject player;
    public GameObject enemy;
    private List<GameObject> enemyList = new List<GameObject>();
    float fltTimeLeft;

    // Start is called before the first frame update
    void Start()
    {
        StartGame();
    }

    // Update is called once per frame
    void Update()
    {
        if (fltTimeLeft == 0)
        {
            StartGame();
        }
    }

    void StartGame()
    {
        player.transform.position = new Vector3(0f, 0f, 0f);

        foreach (GameObject item in enemyList)
        {
            Destroy(item);
        }

        enemyList.Add(Instantiate(enemy, new Vector3(5f, 1f, 3f), Quaternion.identity));
        enemyList.Add(Instantiate(enemy, new Vector3(10f, 1f, 5f), Quaternion.identity));
        enemyList.Add(Instantiate(enemy, new Vector3(7f, 1f, 15f), Quaternion.identity));

        StartCoroutine(TimeCount(10));
    }

    public IEnumerator TimeCount(float timeValue)
    {
        fltTimeLeft = timeValue;
        while (fltTimeLeft > 0)
        {
            Debug.Log(fltTimeLeft + " seconds left!");
            yield return new WaitForSeconds(1.0f);
            fltTimeLeft--;
        }
    }
    
}
