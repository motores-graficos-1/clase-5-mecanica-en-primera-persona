using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_EnemyAI : MonoBehaviour
{
    private int intHp;
    private GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        intHp = 100;
        player = GameObject.Find("Player");
    }

    private void Update()
    {
        transform.LookAt(player.transform);
        transform.Translate(7.0f * Vector3.forward * Time.deltaTime);
    }

    public void TakeDamage()
    {
        intHp -= 25;

        if (intHp <= 0)
        {
            this.Dissapear();
        }
    }

    private void Dissapear()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            TakeDamage();
        }
    }
}
