using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_PlayerControllerRaycast : MonoBehaviour
{
    public Camera playerCamera;
    public int intMaxHp = 15;
    public int intCurrentHp = 15;
    public float fltMovingSpeed = 10f;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked; //Lock the cursor into the center of the screen
    }

    // Update is called once per frame
    void Update()
    {
        float fwrdMov = Input.GetAxis("Vertical") * fltMovingSpeed;
        float sideMov= Input.GetAxis("Horizontal") * fltMovingSpeed;

        fwrdMov *= Time.deltaTime;
        sideMov *= Time.deltaTime;

        transform.Translate(sideMov, 0f, fwrdMov);

        if (Input.GetKeyDown("escape")) //If the player clicks Escape key
        {
            Cursor.lockState = CursorLockMode.None; //Display the cursor on screen
        }

        if (Input.GetMouseButtonDown(0)) //If the player clicks the LMB
        {
            Ray ray = playerCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f)); //Create a Raycast from the center of the screen
            RaycastHit hit; //Create a RaycastHit variable for the raycast
            if ((Physics.Raycast(ray, out hit) == true) && hit.distance<5) { //If you hit something and it is within a 5 units distance
                Debug.Log("Raycast hit object: " + hit.collider.name); //Debug message
                if (hit.collider.name.Substring(0,5) == "Enemy") //If the name of the collider contains "Enemy"
                {
                    GameObject hitObject = GameObject.Find(hit.transform.name); //Get which of the enemies was collided
                    Scr_EnemyAI hitObjectScript = (Scr_EnemyAI)hitObject.GetComponent(typeof(Scr_EnemyAI)); //Get the collided enemy's AI script
                    if (hitObjectScript != null) //If the AI script exists (to avoid crashing if the script component was not added)
                    {
                        hitObjectScript.TakeDamage(); //Execute the take damage method
                    }
                }
            }
        }
    }

    private bool IsDead()
    {
        if (intCurrentHp <= 0)
        {
            return false;
        } else { return true; }
    }
}
