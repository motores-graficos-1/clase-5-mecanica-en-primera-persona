using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_PlayerControllerProjectile : MonoBehaviour
{
    public Camera playerCamera;
    public GameObject projectile;
    public int intMaxHp = 15;
    public int intCurrentHp = 15;
    public float fltMovingSpeed = 10f;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float fwrdMov = Input.GetAxis("Vertical") * fltMovingSpeed;
        float sideMov= Input.GetAxis("Horizontal") * fltMovingSpeed;

        fwrdMov *= Time.deltaTime;
        sideMov *= Time.deltaTime;

        transform.Translate(sideMov, 0f, fwrdMov);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetMouseButtonDown(0)) //If the player clicks the LMB
        {
            Ray ray = playerCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f)); //Create a Raycast from the center of the screen
            GameObject pro;
            pro = Instantiate(projectile, ray.origin, transform.rotation);

            Rigidbody proRb = pro.GetComponent<Rigidbody>();
            proRb.AddForce(playerCamera.transform.forward * 15, ForceMode.Impulse);
            Destroy(pro, 5); //Destroy the bullet in 5 seconds
        }
    }

    private bool IsDead()
    {
        if (intCurrentHp <= 0)
        {
            return false;
        } else { return true; }
    }
}
